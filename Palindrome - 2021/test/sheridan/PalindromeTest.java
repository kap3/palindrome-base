package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindromeRegular( ) {
		boolean test = Palindrome.isPalindrome("racecar");
		assertTrue("Value provided failed palindrome validation...", test);
	}

	@Test
	public void testIsPalindromeException( ) {
		boolean test = Palindrome.isPalindrome("thisfails");
		assertFalse("Value provided...", test);
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		boolean test = Palindrome.isPalindrome("Taco cat");
		assertTrue("Value provided failed palindrome validation...", test);
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		boolean test = Palindrome.isPalindrome("race on car"); //Don't nod --- Was it a cat I saw?
		assertFalse("Value provided...", test);
	}	
	
}
